# Parquet Functions
from collections import OrderedDict
from pyresearch.util.sharedfunctions import *
import pyresearch

docs_dict = OrderedDict()


#   docs_dict[''] = {'fargs':'',
#               'fret': '',
#               'shortdesc': '',
#               'desc': ''
#               }

docs_dict['fixBeakerBool'] = {'fargs':'input_df: The Input Dataframe to fix the boolean fields on ',
          'fret': 'output_df: The input_df variable with the boolean columns converted to Objects for display',
           'shortdesc': 'Convert boolean fields in a Dataframe to object for display in BeakerX',
           'desc': 'fixBeakerBool(input_df)\nBeakerX has a bug when it tries to display a Boolean column it errors out, this function fixes that'
            }


def docs(funcname=None):
    pyresearch.docs(funcname, docs_dict)


