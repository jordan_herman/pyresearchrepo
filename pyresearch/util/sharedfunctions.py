#
import pandas as pd
import pyresearch
def fixBeakerBool(input_df):
    # PUBLIC
    output_df = input_df
    for x in output_df.columns:
        if output_df.dtypes[x] == 'bool':
            output_df[x] = output_df[x].astype(str)
    return output_df

