# Parquet Functions
from collections import OrderedDict
from pyresearch.hostaxis.sharedfunctions import *
import pandas as pd
import pyresearch



docs_dict = OrderedDict()

#   docs_dict[''] = {'fargs':'',
#               'fret': '',
#               'shortdesc': '',
#               'desc': ''
#               }

docs_dict['getparentsbychildlist'] = {'fargs':'mydomains: a list of domains to check for parents\ndebug: show the domain as its looked up',
               'fret': 'output_df: A dataframe that has the results from hostaxis including all parents of the childdomains in the list',
               'shortdesc': 'Check a list of domains for all parent domains in hostaxis',
               'desc': 'getparentsbychildlist(mydomains)\nUsing Hostaxis hosts parents --causes script.src, return the list of all parent URLs where there is a child relationship to one of the domains in the provided host list'
               }


def docs(funcname=None):
    pyresearch.docs(funcname, docs_dict)

